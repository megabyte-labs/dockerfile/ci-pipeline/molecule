# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.1](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/molecule/compare/v3.3.7...v0.1.1) (2021-06-16)

### [3.3.7](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/molecule/compare/v3.3.6...v3.3.7) (2021-06-16)

### [3.3.6](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/molecule/compare/v3.3.5...v3.3.6) (2021-06-16)

### [3.3.5](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/molecule/compare/v3.3.4...v3.3.5) (2021-06-16)

### [3.3.4](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/molecule/compare/v0.0.24...v3.3.4) (2021-06-14)

### [0.0.24](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/molecule/compare/v0.0.23...v0.0.24) (2021-06-13)

### 0.0.23 (2021-05-13)
